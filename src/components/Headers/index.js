import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import './headers.scss';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    position: 'fixed',
    width: '100%'
  },
  menuButton: {
    marginLeft: theme.spacing(2.5),
    color: 'white'
  },
  marginLeft: {
    marginLeft: theme.spacing(2.5)
  },
  title: {
    flexGrow: 1,
  },
  background: {
      backgroundColor: '#d71149'
  },
  container: {
      marginRight: '33px',
      marginLeft: '33px'
  },
  search: {
    padding: '1px 4px',
    marginTop: 6,
    display: 'flex',
    alignItems: 'center',
    width: 406,
    border: 'none',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
    }
}));

export default function Headers() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.background}>
        <Toolbar className={classes.container}>
            <div className="layout_item u-3of12">
                <ul>
                    <li>
                        <a className="logo" href="/">Logo</a>
                    </li>
                    <li className="menu_item mar_left_2">
                        <FormatListBulletedIcon />
                        <strong>Kategori</strong>
                    </li>
                </ul>
            </div>
            <div className="layout_item u-9of12">
                <div className="wrapper">
                    <div>
                        <Paper component="form" className={classes.search}>
                            <InputBase
                                className={classes.input}
                                placeholder="Coba cari masker kain"
                                inputProps={{ 'aria-label': 'search google maps' }}
                            />
                            <Divider orientation="vertical" flexItem />
                            <IconButton type="submit" className={classes.iconButton} aria-label="search">
                                <SearchIcon />
                            </IconButton>
                        </Paper>
                    </div>
                    <div>
                        <ul>
                            <li className="c-nav-menu_transaction">
                                <div className="c-nav-divider"></div>
                                <p className="padding_0_14">Lihat status transaksi</p>
                            </li>
                            <li className="c-nav-menu_transaction">
                                <div className="c-nav-divider"></div>
                                <div className="c-padding-top padding_0_14">
                                    <ShoppingCartIcon />
                                </div>
                            </li>
                            <li className="c-nav-menu_transaction">
                                <div className="c-nav-divider"></div>
                                <div className="c-padding-top">
                                    <Button variant="contained" size="small"  className={classes.marginLeft}>
                                    Login
                                    </Button>
                                    <Button variant="contained" size="small"  className={classes.marginLeft}>
                                    Daftar
                                    </Button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}